/*
 * CONTROLE DE ESCOPO
 * Projeto: Silimed
 * Desenvolvimento: GM5
 */

var attrController = $('main[data-controller]').attr('data-controller');

var Controller = {
    getController: function () {
        if ($('main[data-controller]').length > 0) {
            eval('Controller.' + attrController + '();');
        }
    },
    global: function(){
        
    },
    home: function(){
    },
    

};

jQuery(document).ready(function ($) {
    Controller.global();
    Controller.getController();
    Util.carouselSponsors();
    Util.menuMobile();
});

(console.info || console.log).call(console, "%c<Dev by 🐱 GM5 Team/>", "color: green; font-weight: bold;");
