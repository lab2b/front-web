/*
 * FUNÇÕES NECESSÁRIAS
 * Projeto: Silimed
 * Desenvolvimento: GM5
 */

var Util = {
    aos: function (){
        AOS.init({
            easing: 'ease-in-out-sine',
            duration: 900,
        });
    },
    carouselSponsors: function(){
        $('.responsive').slick({
            
            infinite: true,
            speed: 300,
            autoplay: true,
            arrows: false,            
            slidesToShow: 3,
            slidesToScroll: 1,
            dots:false,
            centerMode: true,
            responsive: [
            {
                breakpoint: 1024,
                settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                }
            },
            {
                breakpoint: 600,
                settings: {
                slidesToShow: 2,
                slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                slidesToShow: 1,
                slidesToScroll: 1
                }
            }
            ]
        });
        
    },
    menuMobile: function(){
        
		menu = $('header nav ul');
			
        $('.mobile button').on('click touchstart', function(e){
			var self = $(this);
			$('html').toggleClass('activeMenu');
			if ( self.hasClass('active') ){
				self.removeClass('active');
				menu.removeClass('active');
				
				// menu.removeClass('d-block');
				// menu.addClass('d-none');
			}else{
				self.addClass('active');
				menu.addClass('active');
				
				// menu.removeClass('d-none');
				// menu.addClass('d-block');
			}
			e.preventDefault();
		});
		
        // Fecha Menu se fizer resize acima de resolução Tablet
        $(window).resize(function() {
            if ($(window).width() > 992 && $('.btnMobile').hasClass('active')){                
                $('.btnMobile').trigger('click');
            }
        });
        
        // Fecha Menu se clicar em link do menu
        menu.on('click touch', 'li a', function(){
            $('.btnMobile').trigger('click');
            console.log('debug')
        }) 
    },


}